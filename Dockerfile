FROM alpine

RUN apk add proftpd shadow

RUN usermod --shell /bin/ash ftp

RUN mkdir /home/ftp && chown -R ftp:ftp /home/ftp

RUN chown -R ftp:ftp /var/lib/ftp

RUN mkdir /run/proftpd

COPY proftpd.conf /etc/proftpd/proftpd.conf

EXPOSE 21

CMD ["proftpd", "--debug", "10", "--nodaemon"]
